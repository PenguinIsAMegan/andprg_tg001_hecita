﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnManager : MonoBehaviour
{
    // Spawning of enemy
    // Spawnrate
    // Randomize Enemy Spawning Location and Enemy Type

    public GameObject enemy1;
    public GameObject spawnPoint;

    public float spawnTimer;
    public float spawnRate;

    void Start()
    {
        spawnRate = Random.Range(2, 6);
    }
    // Update is called once per frame
    void Update()
    {
        spawnTimer += 1 * Time.deltaTime;
       

        if (spawnTimer >= spawnRate)
        {
            spawnRate = Random.Range(2, 6);
            Debug.Log("spawnRate: " + spawnRate);
            spawnTimer = 0;
            Spawn();
        }
    }

    public void Spawn()
    {
        float randVal = Random.Range(-5, 5);
        Vector3 spawnPos = new Vector3(randVal + spawnPoint.transform.position.x,
                                       spawnPoint.transform.position.y,
                                       spawnPoint.transform.position.z);
        Instantiate(enemy1.gameObject, spawnPos, Quaternion.identity);
    }
}
