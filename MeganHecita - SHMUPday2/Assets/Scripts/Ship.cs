﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Ship : MonoBehaviour
{
    public float speed; // Variable with 'public' can be seen in Unity's Inspector
    public GameObject Bullet;
    //TO DO:
    // Up and Down Movement
    // Press Space Bar then print "Fire!"

    // Start is called before the first frame update
    void Start()
    {

    }

    // Awake is called during initialisation, when initialisation is complete, Start will be called
    private void Awake()
    {

    }

    // Update is called once per frame
    void Update()
    {
        // Debug.Log("Ship Update!!!");

        // Left Right Movement
        float transition = Input.GetAxis("Horizontal") * speed * Time.deltaTime; // Time.deltaTime - for same performance crossplatform 
        this.transform.position += new Vector3(transition, 0, 0);

        // Up Down Movement
        transition = Input.GetAxis("Vertical") * speed * Time.deltaTime; // Time.deltaTime - for same performance crossplatform 
        this.transform.position += new Vector3(0, transition, 0);

        // Press Space
        if (Input.GetKeyDown(KeyCode.Space))
        {
            Instantiate(Bullet, this.transform.position + this.transform.up * 2, Quaternion.identity);
        }
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("Enemy"))
        {
            Destroy(this.gameObject);
        }
    }
}
