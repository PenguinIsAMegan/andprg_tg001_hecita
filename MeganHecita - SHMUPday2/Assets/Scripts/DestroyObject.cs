﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DestroyObject : MonoBehaviour
{
    public float spawnTimer;

    // Update is called once per frame
    void Update()
    {
        spawnTimer += 1 * Time.deltaTime;

        if (spawnTimer > 5)
        {
            spawnTimer = 0;
            Destroy(this.gameObject);
        }
    }
}
