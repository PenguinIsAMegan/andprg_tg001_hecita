﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class Player : MonoBehaviour
{
    public float speed;
    public int TotalCoinsCollected = 0;
    private Vector3 currentRotation;
    public Vector3 angleRotation;
    private float screenHeight = 86;
    private float screenWidth = 214;
    public Text counter;

    // Start is called before the first frame update
    void Start()
    {
      
    }

    // Update is called once per frame
    void Update()
    {
        PlayerControls();
        ScreenWrap();
    }

    private void ScreenWrap()
    {
        Vector3 playerPos = transform.position;

        if (transform.position.y > screenHeight)
        {
            playerPos.y = -screenHeight;
        }

        if (transform.position.y < -screenHeight)
        {
            playerPos.y = screenHeight;
        }

        if (transform.position.x > screenWidth)
        {
            playerPos.x = -screenWidth;
        }

        if (transform.position.x < -screenWidth)
        {
            playerPos.x = screenWidth;
        }

        playerPos.z = -63.10001f;
        transform.position = playerPos;
    }

    private void PlayerControls()
    {
        transform.position += transform.right * speed * Time.deltaTime;

        if (Input.GetKey(KeyCode.A))
        {
            currentRotation = currentRotation + angleRotation * Time.deltaTime;
            this.transform.eulerAngles = currentRotation;
        }

        if (Input.GetKey(KeyCode.D))
        {
            currentRotation = currentRotation + -angleRotation * Time.deltaTime;
            this.transform.eulerAngles = currentRotation;
        }

        if (Input.GetKey(KeyCode.W))
        {
            this.transform.localScale += new Vector3(0.5f, 0.5f, 0.5f);
        }

        if (Input.GetKey(KeyCode.S))
        {
            this.transform.localScale -= new Vector3(0.5f, 0.5f, 0.5f);

            if (this.transform.localScale.x < 0 && this.transform.localScale.y < 0 && this.transform.localScale.z < 0)
            {
                this.transform.localScale = new Vector3(1.0f, 1.0f, 1.0f);
            }
        }
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("Coin"))
        {
            Destroy(collision.gameObject);
            GameObject.Find("SpawnManager").GetComponent<SpawnManager>().SpawnCoin();
        }

        else if (collision.gameObject.CompareTag("Enemy"))
        {
            Destroy(this.gameObject);
            Destroy(collision.gameObject);
            SceneManager.LoadScene("End");
        }
    }

    public void addCoins(int amount)
    {
        TotalCoinsCollected += amount;
        counter.text = TotalCoinsCollected.ToString();
    }

    public int getCoins()
    {
        return TotalCoinsCollected;
    }
}
