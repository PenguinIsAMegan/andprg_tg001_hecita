﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy : MonoBehaviour
{
    public float speed;
    private float scaleX, scaleY, scaleZ;

    // Start is called before the first frame update
    void Start()
    {
        scaleX = Random.Range(0, 5.0f);
        scaleY = Random.Range(0, 5.0f);
        scaleZ = Random.Range(0, 5.0f);
        int randVal = Random.Range(0, 1);

        if (randVal == 0)
        {
            this.transform.localScale += new Vector3(scaleX, scaleY, scaleZ);
        }

        else
        {
            this.transform.localScale -= new Vector3(scaleX, scaleY, scaleZ);
        }

        speed = Random.Range(50, 100);
    }

    // Update is called once per frame
    void Update()
    {
        transform.position += transform.right * speed * Time.deltaTime;
    }
}
