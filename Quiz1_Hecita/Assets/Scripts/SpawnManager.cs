﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnManager : MonoBehaviour
{
    public GameObject coin;
    public GameObject LRPoint;
    public GameObject TBPoint;
    public GameObject enemy;
    public float spawnTimer, spawnRate;
    private Vector3 currentRotation;

    // Start is called before the first frame update
    void Start()
    {
        SpawnCoin();
        SpawnEnemy();
    }

    // Update is called once per frame
    void Update()
    {
        spawnTimer += 1 * Time.deltaTime;

        if (spawnTimer >= spawnRate)
        {
            SpawnEnemy();
            spawnTimer = 0;
        }
    }

    public void SpawnCoin()
    {
        Vector3 pos = new Vector3(Random.value, Random.value, 10.0f);

        pos = Camera.main.ViewportToWorldPoint(pos);
        Instantiate(coin, pos, Quaternion.identity);
    }

    public void SpawnEnemy()
    {
        float randVal = Random.Range(0, 3);

        if (randVal == 0)
        {
            randVal = Random.Range(-75, 75);
            Vector3 spawnPos = new Vector3(-LRPoint.transform.position.x, randVal + LRPoint.transform.position.y, LRPoint.transform.position.z);
            Instantiate(enemy, spawnPos, Quaternion.identity);
        }

        else if (randVal == 1)
        {
            randVal = Random.Range(-75, 75);
            Vector3 spawnPos = new Vector3(LRPoint.transform.position.x, randVal + LRPoint.transform.position.y, LRPoint.transform.position.z);
            Quaternion spawnRotation = Quaternion.Euler(0, 0, 180);
            Instantiate(enemy, spawnPos, spawnRotation);
        }

        else if (randVal == 2)
        {
            randVal = Random.Range(-170, 170);
            Vector3 spawnPos = new Vector3(randVal + TBPoint.transform.position.x, TBPoint.transform.position.y, TBPoint.transform.position.z);
            Quaternion spawnRotation = Quaternion.Euler(0, 0, -90);
            Instantiate(enemy, spawnPos, spawnRotation);
        }

        else if (randVal == 3)
        {
            randVal = Random.Range(-170, 170);
            Vector3 spawnPos = new Vector3(randVal + TBPoint.transform.position.x, -TBPoint.transform.position.y, TBPoint.transform.position.z);
            Quaternion spawnRotation = Quaternion.Euler(0, 0, 90);
            Instantiate(enemy, spawnPos, spawnRotation);
        }
    }
}
